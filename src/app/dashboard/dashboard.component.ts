import { Component, OnInit, ElementRef, HostListener } from '@angular/core';
import { AfterViewInit } from '@angular/core'
import { Chart, registerables } from "chart.js";

declare var $: any;
declare var Gauge: any;
declare const jQuery: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit, AfterViewInit {
  chart: Chart;
  constructor() { }

  init_JQVmap() {

    var sample_data = {
      "AF": 16.63,
      "AL": 11.58,
      "DZ": 158.97,

    }

    var data: [{
      name: 'Afghanistan',
      value: 28397.812
    }, {
      name: 'Angola',
      value: 19549.124
    }, {
      name: 'Albania',
      value: 3150.143
    }, {
      name: 'United Arab Emirates',
      value: 8441.537
    }, {
      name: 'Argentina',
      value: 40374.224
    }, {
      name: 'Armenia',
      value: 2963.496
    }, {
      name: 'French Southern and Antarctic Lands',
      value: 268.065
    }, {
      name: 'Australia',
      value: 22404.488
    }, {
      name: 'Austria',
      value: 8401.924
    }, {
      name: 'Azerbaijan',
      value: 9094.718
    }, {
      name: 'Burundi',
      value: 9232.753
    }, {
      name: 'Belgium',
      value: 10941.288
    }, {
      name: 'Benin',
      value: 9509.798
    }, {
      name: 'Burkina Faso',
      value: 15540.284
    }, {
      name: 'Bangladesh',
      value: 151125.475
    }, {
      name: 'Bulgaria',
      value: 7389.175
    }, {
      name: 'The Bahamas',
      value: 66402.316
    }, {
      name: 'Bosnia and Herzegovina',
      value: 3845.929
    }, {
      name: 'Belarus',
      value: 9491.07
    }, {
      name: 'Belize',
      value: 308.595
    }, {
      name: 'Bermuda',
      value: 64.951
    }, {
      name: 'Bolivia',
      value: 716.939
    }, {
      name: 'Brazil',
      value: 195210.154
    }, {
      name: 'Brunei',
      value: 27.223
    }, {
      name: 'Bhutan',
      value: 716.939
    }, {
      name: 'Botswana',
      value: 1969.341
    }, {
      name: 'Central African Republic',
      value: 4349.921
    }, {
      name: 'Canada',
      value: 34126.24
    }, {
      name: 'Switzerland',
      value: 7830.534
    }, {
      name: 'Chile',
      value: 17150.76
    }, {
      name: 'China',
      value: 1359821.465
    }, {
      name: 'Ivory Coast',
      value: 60508.978
    }, {
      name: 'Cameroon',
      value: 20624.343
    }, {
      name: 'Democratic Republic of the Congo',
      value: 62191.161
    }, {
      name: 'Republic of the Congo',
      value: 3573.024
    }, {
      name: 'Colombia',
      value: 46444.798
    }, {
      name: 'Costa Rica',
      value: 4669.685
    }, {
      name: 'Cuba',
      value: 11281.768
    }, {
      name: 'Northern Cyprus',
      value: 1.468
    }, {
      name: 'Cyprus',
      value: 1103.685
    }, {
      name: 'Czech Republic',
      value: 10553.701
    }, {
      name: 'Germany',
      value: 83017.404
    }, {
      name: 'Djibouti',
      value: 834.036
    }, {
      name: 'Denmark',
      value: 5550.959
    }, {
      name: 'Dominican Republic',
      value: 10016.797
    }, {
      name: 'Algeria',
      value: 37062.82
    }, {
      name: 'Ecuador',
      value: 15001.072
    }, {
      name: 'Egypt',
      value: 78075.705
    }, {
      name: 'Eritrea',
      value: 5741.159
    }, {
      name: 'Spain',
      value: 46182.038
    }, {
      name: 'Estonia',
      value: 1298.533
    }, {
      name: 'Ethiopia',
      value: 87095.281
    }, {
      name: 'Finland',
      value: 5367.693
    }, {
      name: 'Fiji',
      value: 860.559
    }, {
      name: 'Falkland Islands',
      value: 49.581
    }, {
      name: 'France',
      value: 63230.866
    }, {
      name: 'Gabon',
      value: 1556.222
    }, {
      name: 'United Kingdom',
      value: 62066.35
    }, {
      name: 'Georgia',
      value: 4388.674
    }, {
      name: 'Ghana',
      value: 24262.901
    }, {
      name: 'Guinea',
      value: 10876.033
    }, {
      name: 'Gambia',
      value: 1680.64
    }, {
      name: 'Guinea Bissau',
      value: 10876.033
    }, {
      name: 'Equatorial Guinea',
      value: 696.167
    }, {
      name: 'Greece',
      value: 11109.999
    }, {
      name: 'Greenland',
      value: 56.546
    }, {
      name: 'Guatemala',
      value: 14341.576
    }, {
      name: 'French Guiana',
      value: 231.169
    }, {
      name: 'Guyana',
      value: 786.126
    }, {
      name: 'Honduras',
      value: 7621.204
    }, {
      name: 'Croatia',
      value: 4338.027
    }, {
      name: 'Haiti',
      value: 9896.4
    }, {
      name: 'Hungary',
      value: 10014.633
    }, {
      name: 'Indonesia',
      value: 240676.485
    }, {
      name: 'India',
      value: 1205624.648
    }, {
      name: 'Ireland',
      value: 4467.561
    }, {
      name: 'Iran',
      value: 240676.485
    }, {
      name: 'Iraq',
      value: 30962.38
    }, {
      name: 'Iceland',
      value: 318.042
    }, {
      name: 'Israel',
      value: 7420.368
    }, {
      name: 'Italy',
      value: 60508.978
    }, {
      name: 'Jamaica',
      value: 2741.485
    }, {
      name: 'Jordan',
      value: 6454.554
    }, {
      name: 'Japan',
      value: 127352.833
    }, {
      name: 'Kazakhstan',
      value: 15921.127
    }, {
      name: 'Kenya',
      value: 40909.194
    }, {
      name: 'Kyrgyzstan',
      value: 5334.223
    }, {
      name: 'Cambodia',
      value: 14364.931
    }, {
      name: 'South Korea',
      value: 51452.352
    }, {
      name: 'Kosovo',
      value: 97.743
    }, {
      name: 'Kuwait',
      value: 2991.58
    }, {
      name: 'Laos',
      value: 6395.713
    }, {
      name: 'Lebanon',
      value: 4341.092
    }, {
      name: 'Liberia',
      value: 3957.99
    }, {
      name: 'Libya',
      value: 6040.612
    }, {
      name: 'Sri Lanka',
      value: 20758.779
    }, {
      name: 'Lesotho',
      value: 2008.921
    }, {
      name: 'Lithuania',
      value: 3068.457
    }, {
      name: 'Luxembourg',
      value: 507.885
    }, {
      name: 'Latvia',
      value: 2090.519
    }, {
      name: 'Morocco',
      value: 31642.36
    }, {
      name: 'Moldova',
      value: 103.619
    }, {
      name: 'Madagascar',
      value: 21079.532
    }, {
      name: 'Mexico',
      value: 117886.404
    }, {
      name: 'Macedonia',
      value: 507.885
    }, {
      name: 'Mali',
      value: 13985.961
    }, {
      name: 'Myanmar',
      value: 51931.231
    }, {
      name: 'Montenegro',
      value: 620.078
    }, {
      name: 'Mongolia',
      value: 2712.738
    }, {
      name: 'Mozambique',
      value: 23967.265
    }, {
      name: 'Mauritania',
      value: 3609.42
    }, {
      name: 'Malawi',
      value: 15013.694
    }, {
      name: 'Malaysia',
      value: 28275.835
    }, {
      name: 'Namibia',
      value: 2178.967
    }, {
      name: 'New Caledonia',
      value: 246.379
    }, {
      name: 'Niger',
      value: 15893.746
    }, {
      name: 'Nigeria',
      value: 159707.78
    }, {
      name: 'Nicaragua',
      value: 5822.209
    }, {
      name: 'Netherlands',
      value: 16615.243
    }, {
      name: 'Norway',
      value: 4891.251
    }, {
      name: 'Nepal',
      value: 26846.016
    }, {
      name: 'New Zealand',
      value: 4368.136
    }, {
      name: 'Oman',
      value: 2802.768
    }, {
      name: 'Pakistan',
      value: 173149.306
    }, {
      name: 'Panama',
      value: 3678.128
    }, {
      name: 'Peru',
      value: 29262.83
    }, {
      name: 'Philippines',
      value: 93444.322
    }, {
      name: 'Papua New Guinea',
      value: 6858.945
    }, {
      name: 'Poland',
      value: 38198.754
    }, {
      name: 'Puerto Rico',
      value: 3709.671
    }, {
      name: 'North Korea',
      value: 1.468
    }, {
      name: 'Portugal',
      value: 10589.792
    }, {
      name: 'Paraguay',
      value: 6459.721
    }, {
      name: 'Qatar',
      value: 1749.713
    }, {
      name: 'Romania',
      value: 21861.476
    }, {
      name: 'Russia',
      value: 21861.476
    }, {
      name: 'Rwanda',
      value: 10836.732
    }, {
      name: 'Western Sahara',
      value: 514.648
    }, {
      name: 'Saudi Arabia',
      value: 27258.387
    }, {
      name: 'Sudan',
      value: 35652.002
    }, {
      name: 'South Sudan',
      value: 9940.929
    }, {
      name: 'Senegal',
      value: 12950.564
    }, {
      name: 'Solomon Islands',
      value: 526.447
    }, {
      name: 'Sierra Leone',
      value: 5751.976
    }, {
      name: 'El Salvador',
      value: 6218.195
    }, {
      name: 'Somaliland',
      value: 9636.173
    }, {
      name: 'Somalia',
      value: 9636.173
    }, {
      name: 'Republic of Serbia',
      value: 3573.024
    }, {
      name: 'Suriname',
      value: 524.96
    }, {
      name: 'Slovakia',
      value: 5433.437
    }, {
      name: 'Slovenia',
      value: 2054.232
    }, {
      name: 'Sweden',
      value: 9382.297
    }, {
      name: 'Swaziland',
      value: 1193.148
    }, {
      name: 'Syria',
      value: 7830.534
    }, {
      name: 'Chad',
      value: 11720.781
    }, {
      name: 'Togo',
      value: 6306.014
    }, {
      name: 'Thailand',
      value: 66402.316
    }, {
      name: 'Tajikistan',
      value: 7627.326
    }, {
      name: 'Turkmenistan',
      value: 5041.995
    }, {
      name: 'East Timor',
      value: 10016.797
    }, {
      name: 'Trinidad and Tobago',
      value: 1328.095
    }, {
      name: 'Tunisia',
      value: 10631.83
    }, {
      name: 'Turkey',
      value: 72137.546
    }, {
      name: 'United Republic of Tanzania',
      value: 44973.33
    }, {
      name: 'Uganda',
      value: 33987.213
    }, {
      name: 'Ukraine',
      value: 46050.22
    }, {
      name: 'Uruguay',
      value: 3371.982
    }, {
      name: 'United States of America',
      value: 312247.116
    }, {
      name: 'Uzbekistan',
      value: 27769.27
    }, {
      name: 'Venezuela',
      value: 236.299
    }, {
      name: 'Vietnam',
      value: 89047.397
    }, {
      name: 'Vanuatu',
      value: 236.299
    }, {
      name: 'West Bank',
      value: 13.565
    }, {
      name: 'Yemen',
      value: 22763.008
    }, {
      name: 'South Africa',
      value: 51452.352
    }, {
      name: 'Zambia',
      value: 13216.985
    }, {
      name: 'Zimbabwe',
      value: 13076.978
    }]

    // console.log('check init_JQVmap [' + typeof (VectorCanvas) + '][' + typeof (jQuery.fn.vectorMap) + ']' );	

    if (typeof (jQuery.fn.vectorMap) === 'undefined') { return; }

    console.log('init_JQVmap');

    if ($('#world-map-gdp').length) {

      jQuery('#world-map-gdp').vectorMap({
        map: 'world_en',
        backgroundColor: null,
        color: '#ffffff',
        hoverOpacity: 0.7,
        selectedColor: '#666666',
        enableZoom: true,
        showTooltip: true,
        values: sample_data,
        scaleColors: ['#E6F2F0', '#149B7E'],
        normalizeFunction: 'polynomial'
      });

    }



  };

  init_gauge() {

    if (typeof (Gauge) === 'undefined') { return; }

    console.log('init_gauge [' + $('.gauge-chart').length + ']');

    console.log('init_gauge');


    var chart_gauge_settings = {
      lines: 12,
      angle: 0,
      lineWidth: 0.4,
      pointer: {
        length: 0.75,
        strokeWidth: 0.042,
        color: '#1D212A'
      },
      limitMax: 'false',
      colorStart: '#1ABC9C',
      colorStop: '#1ABC9C',
      strokeColor: '#F0F3F3',
      generateGradient: true
    };


    if ($('#chart_gauge_01').length) {

      var chart_gauge_01_elem = document.getElementById('chart_gauge_01');
      var chart_gauge_01 = new Gauge(chart_gauge_01_elem).setOptions(chart_gauge_settings);

    }


    if ($('#gauge-text').length) {

      chart_gauge_01.maxValue = 6000;
      chart_gauge_01.animationSpeed = 32;
      chart_gauge_01.set(3200);
      chart_gauge_01.setTextField(document.getElementById("gauge-text"));

    }

    if ($('#chart_gauge_02').length) {

      var chart_gauge_02_elem = document.getElementById('chart_gauge_02');
      var chart_gauge_02 = new Gauge(chart_gauge_02_elem).setOptions(chart_gauge_settings);

    }


    if ($('#gauge-text2').length) {

      chart_gauge_02.maxValue = 9000;
      chart_gauge_02.animationSpeed = 32;
      chart_gauge_02.set(2400);
      chart_gauge_02.setTextField(document.getElementById("gauge-text2"));

    }


  }
  ngAfterViewInit(): void {

    this.init_flot_chart();
    // this.init_chart_doughnut();
    this.doughnut_chart();
    this.init_gauge();
    this.init_JQVmap();
  }
  gd(year: any, month: any, day: any) {
    return new Date(year, month - 1, day).getTime();
  }

  init_flot_chart() {

    if (typeof ($.plot) === 'undefined') { return; }

    console.log('init_flot_chart');
    var randNum = function () {
      return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
    };

    var arr_data1 = [
      [this.gd(2012, 1, 1), 17],
      [this.gd(2012, 1, 2), 74],
      [this.gd(2012, 1, 3), 6],
      [this.gd(2012, 1, 4), 39],
      [this.gd(2012, 1, 5), 20],
      [this.gd(2012, 1, 6), 85],
      [this.gd(2012, 1, 7), 7]
    ];

    var arr_data2 = [
      [this.gd(2012, 1, 1), 82],
      [this.gd(2012, 1, 2), 23],
      [this.gd(2012, 1, 3), 66],
      [this.gd(2012, 1, 4), 9],
      [this.gd(2012, 1, 5), 119],
      [this.gd(2012, 1, 6), 6],
      [this.gd(2012, 1, 7), 9]
    ];

    var arr_data3 = [
      [0, 1],
      [1, 9],
      [2, 6],
      [3, 10],
      [4, 5],
      [5, 17],
      [6, 6],
      [7, 10],
      [8, 7],
      [9, 11],
      [10, 35],
      [11, 9],
      [12, 12],
      [13, 5],
      [14, 3],
      [15, 4],
      [16, 9]
    ];

    var chart_plot_02_data = [];

    var chart_plot_03_data = [
      [0, 1],
      [1, 9],
      [2, 6],
      [3, 10],
      [4, 5],
      [5, 17],
      [6, 6],
      [7, 10],
      [8, 7],
      [9, 11],
      [10, 35],
      [11, 9],
      [12, 12],
      [13, 5],
      [14, 3],
      [15, 4],
      [16, 9]
    ];

    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();

    var todayl = mm + '/' + dd + '/' + yyyy;

    for (var i = 0; i < 30; i++) {

      // chart_plot_02_data.push([new Date(Date.today().add(i).days()).getTime(), randNum() + i + i + 10]);
      chart_plot_02_data.push(todayl);

    }


    var chart_plot_01_settings = {
      series: {
        lines: {
          show: false,
          fill: true
        },
        splines: {
          show: true,
          tension: 0.4,
          lineWidth: 1,
          fill: 0.4
        },
        points: {
          radius: 0,
          show: true
        },
        shadowSize: 2
      },
      grid: {
        verticalLines: true,
        hoverable: true,
        clickable: true,
        tickColor: "#d5d5d5",
        borderWidth: 1,
        color: '#fff'
      },
      colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
      xaxis: {
        tickColor: "rgba(51, 51, 51, 0.06)",
        mode: "time",
        tickSize: [1, "day"],
        //tickLength: 10,
        axisLabel: "Date",
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 12,
        axisLabelFontFamily: 'Verdana, Arial',
        axisLabelPadding: 10
      },
      yaxis: {
        ticks: 8,
        tickColor: "rgba(51, 51, 51, 0.06)",
      },
      tooltip: false
    }

    var chart_plot_02_settings = {
      grid: {
        show: true,
        aboveData: true,
        color: "#3f3f3f",
        labelMargin: 10,
        axisMargin: 0,
        borderWidth: 0,
        borderColor: "",
        minBorderMargin: 5,
        clickable: true,
        hoverable: true,
        autoHighlight: true,
        mouseActiveRadius: 100
      },
      series: {
        lines: {
          show: true,
          fill: true,
          lineWidth: 2,
          steps: false
        },
        points: {
          show: true,
          radius: 4.5,
          symbol: "circle",
          lineWidth: 3.0
        }
      },
      legend: {
        position: "ne",
        margin: [0, -25],
        noColumns: 0,
        labelBoxBorderColor: "",
        labelFormatter: function (label: any, series: any) {
          return label + '&nbsp;&nbsp;';
        },
        width: 40,
        height: 1
      },
      colors: ['#96CA59', '#3F97EB', '#72c380', '#6f7a8a', '#f7cb38', '#5a8022', '#2c7282'],
      shadowSize: 0,
      tooltip: true,
      tooltipOpts: {
        content: "%s: %y.0",
        xDateFormat: "%d/%m",
        shifts: {
          x: -30,
          y: -50
        },
        defaultTheme: false
      },
      yaxis: {
        min: 0
      },
      xaxis: {
        mode: "time",
        minTickSize: [1, "day"],
        timeformat: "%d/%m/%y",
        min: chart_plot_02_data[0][0],
        max: chart_plot_02_data[20][0]
      }
    };

    var chart_plot_03_settings = {
      series: {
        curvedLines: {
          apply: true,
          active: true,
          monotonicFit: true
        }
      },
      colors: ["#26B99A"],
      grid: {
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 1,
          left: 1
        },
        borderColor: {
          bottom: "#7F8790",
          left: "#7F8790"
        }
      }
    };


    if ($("#chart_plot_01").length) {
      console.log('Plot1');

      $.plot($("#chart_plot_01"), [arr_data1, arr_data2], chart_plot_01_settings);
    }


    if ($("#chart_plot_02").length) {
      console.log('Plot2');

      $.plot($("#chart_plot_02"),
        [{
          label: "Email Sent",
          data: chart_plot_02_data,
          lines: {
            fillColor: "rgba(150, 202, 89, 0.12)"
          },
          points: {
            fillColor: "#fff"
          }
        }], chart_plot_02_settings);

    }

    if ($("#chart_plot_03").length) {
      console.log('Plot3');


      $.plot($("#chart_plot_03"), [{
        label: "Registrations",
        data: chart_plot_03_data,
        lines: {
          fillColor: "rgba(150, 202, 89, 0.12)"
        },
        points: {
          fillColor: "#fff"
        }
      }], chart_plot_03_settings);

    };

  }

  init_chart_doughnut() {

    if (typeof (Chart) === 'undefined') { return; }

    console.log('init_chart_doughnut');

    if ($('.canvasDoughnut').length) {

      var chart_doughnut_settings = {
        type: 'doughnut',
        tooltipFillColor: "rgba(51, 51, 51, 0.55)",
        data: {
          labels: [
            "Symbian",
            "Blackberry",
            "Other",
            "Android",
            "IOS"
          ],
          datasets: [{
            data: [15, 20, 30, 10, 30],
            backgroundColor: [
              "#BDC3C7",
              "#9B59B6",
              "#E74C3C",
              "#26B99A",
              "#3498DB"
            ],
            hoverBackgroundColor: [
              "#CFD4D8",
              "#B370CF",
              "#E95E4F",
              "#36CAAB",
              "#49A9EA"
            ]
          }]
        },
        options: {
          legend: false,
          responsive: false
        }
      }

      $('.canvasDoughnut').each(function () {

        var chart_element = $(this);
        var chart_doughnut = new Chart(chart_element, this.chart_doughnut_settings);

      });

    }

  }


  doughnut_chart() {
    Chart.register(...registerables);
    if ($('#canvasDoughnut').length) {
      const canvasDoughnut = document.getElementById('canvasDoughnut') as HTMLCanvasElement;
      canvasDoughnut.width = 200;
      canvasDoughnut.height = 150;
      const ctxCanvasDoughnut = canvasDoughnut.getContext('2d');
      var data = {
        labels: [
          "Symbian",
          "Blackberry",
          "Other",
          "Android",
          "IOS"
        ],
        datasets: [{
          data: [15, 20, 30, 10, 30],
          backgroundColor: [
            "#BDC3C7",
            "#9B59B6",
            "#E74C3C",
            "#26B99A",
            "#3498DB"
          ],
          hoverBackgroundColor: [
            "#CFD4D8",
            "#B370CF",
            "#E95E4F",
            "#36CAAB",
            "#49A9EA"
          ]

        }]
      };

      new Chart(ctxCanvasDoughnut, {
        type: 'doughnut',
        data: data,

        options: {
          responsive: true,

          plugins: {

            legend: {
              position: 'top',
            },
            title: {
              display: true,
              text: 'Chart.js Doughnut Chart'
            }
          }
        },
      });

    }
  }
  ngOnInit(): void {
  }

}
