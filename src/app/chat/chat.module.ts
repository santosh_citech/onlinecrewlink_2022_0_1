import { NgModule } from '@angular/core';

import { ChatRoutingModule } from "./chat.routing"

import { ChatComponent } from "./chat.component";

import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';  
@NgModule({
    imports: [ChatRoutingModule,CommonModule,FormsModule,ReactiveFormsModule],
    declarations: [ChatComponent],
    exports: [ChatComponent],
    providers: []

})
export class ChatModule { }