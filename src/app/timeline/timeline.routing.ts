import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";


import { TimeLineComponent } from "./timeline.component";
const routes: Routes = [
    { path:"", component: TimeLineComponent },
   
];

@NgModule({
    exports: [RouterModule],
    imports:[RouterModule.forChild(routes)]
})

export class TimeLineRoutingModule{}