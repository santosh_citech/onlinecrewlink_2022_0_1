import { NgModule } from '@angular/core';

import { TimeLineRoutingModule } from "./timeline.routing"

import { TimeLineComponent } from "./timeline.component";

import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';  
@NgModule({
    imports: [TimeLineRoutingModule,CommonModule,FormsModule,ReactiveFormsModule],
    declarations: [TimeLineComponent],
    exports: [TimeLineComponent],
    providers: []

})
export class TimeLineModule { }