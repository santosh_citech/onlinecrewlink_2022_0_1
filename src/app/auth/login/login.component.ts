import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../shared/services/authentication.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: any = {};
  loading = false;
  returnUrl: string = "";
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService


  ) { }

  ngOnInit(): void {
    this.authenticationService.logout();

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  gotoRegister() {
    this.router.navigate(['./home']);
  }

  login() {


    this.authenticationService.login(this.model.username, this.model.password).subscribe({
      next: (result: any) => {
        window.sessionStorage["token"] = result.token.token;
        window.sessionStorage["user"] = result.token.userobj.username; 
        window.sessionStorage["userRole"] = result.token.userobj.role;

        localStorage.setItem("currentUser", JSON.stringify(result));
        this.router.navigate(['/home/dashboard']);
      },

      complete: () => {

      }, // completeHandler
      error: () => { },    // errorHandler 


    });
  }
  reloadPage(): void {
    window.location.reload();
  }

}


