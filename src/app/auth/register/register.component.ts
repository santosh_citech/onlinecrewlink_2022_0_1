import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';

import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../shared/services/authentication.service';
declare var $: any;

export interface IUser {

  _id?: number
  username: string,
  firstname: string,
  lastname: string,
  password: string,
  email: string,
  markdelete: false
  createdtime: Date
  isUserchecked?: boolean;
}
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public model: any = {
    code: "admin"
  };
  public usersList: IUser[] = [];
  public isShowTop: boolean = true;
  //@ViewChild('RegForm', {static: false}) RegForm: NgForm;
  isShow: boolean = true;
  public roleCodesList: string[] = ["admin", "subscriber", "editor", "author", "contributor"];

  itemsPerPage: number = 10;
  currentPage: number = 1;
  isLoading: boolean = false;
  @ViewChild('f') f: any;

  query: any = {
    page: "",
    limit: "",
    order: "",
  }

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit(): void {
    this.getUsers();
  }

  password() {
    this.isShow = !this.isShow;
  }
  getUserById(user: any) {
    if (user !== null && user !== undefined) {
      this.isShowTop = false;
      const token = user.token.slice(0, 10);
      user.token = token;
      Object.assign(this.model, user);
    }
  }

  updateUser(user: any) {
    if (user !== null && user !== undefined) {
      Object.assign(this.model, user);
    }
  }

  update() {


  }

  addUser(RegForm: NgForm) {

    this.authenticationService.registerUser(this.model).subscribe((result) => {
      this.router.navigate(['/login']);
    }, error => {

    })


  }

  removeUser(user: any = {}) {

  }
  selectUser(user: any = {}) {

  }


  getUsers() {
    this.isLoading = true;
    this.authenticationService.getUsers(this.query).subscribe((data) => {
      this.usersList = data.docs;
      this.isLoading = false;
      console.log(data);
    }, error => {

    })
  }

  ngAfterViewInit() {
    this.f.form.valueChanges.subscribe((change: any) => {
      console.log(change)
      this.query.limit = change.limitValue;
      this.query.page = change.pageValue;
      this.getUsers();
    })
  }
}
