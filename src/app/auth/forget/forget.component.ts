import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../shared/services/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-forget',
  templateUrl: './forget.component.html',
  styleUrls: ['./forget.component.css']
})
export class ForgetComponent implements OnInit {
  public model: any = {
    email: ""
  };

  changemodel: any = {}
  errormessage: string = "";
  private queryParams: string = '';
  public ismatch:boolean = false;
  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe((queryParam: any) => {

      console.log(queryParam["id"]);
    });
  }

  validatePassword(event: any) {
    console.log(event);
    if (this.changemodel.newpass !== this.changemodel.renewpass) {
      console.log("inside ()")
      this.ismatch = true;

    }else{
      this.ismatch = false;
    }
    

  }

  resetUserPassword() {

    this.authenticationService.resetUserPassword(this.model).subscribe((result) => {
      this.router.navigate(['/login']);
    }, error => {
      console.log(error);
      this.errormessage = error.error.message;

    })


  }

}
