import { Component, OnInit } from '@angular/core';
import { RoleService } from "../role.service";
import { Subscription } from 'rxjs';
import { timer } from 'rxjs';

export interface IUser {

  _id?: number
  username: string,
  firstname: string,
  lastname: string,
  password: string,
  email: string,
  markdelete: false
  createdtime: Date
  isUserchecked?: boolean;
}
@Component({
  selector: 'app-createrole',
  templateUrl: './createrole.component.html',
  styleUrls: ['./createrole.component.css']
})
export class CreateroleComponent implements OnInit {

  userdetails: any = {
    role: "",
    roleType: "",
  };
  query: any = {
    page: 1,
    size: 10
  }
  roles: any = [];
  usersList: IUser[] = [];
  itemsPerPage: any = 50;
  currentPage: any = 1;
  isSelected: boolean;
  query0: any = {
    page: this.currentPage,
    limit: this.itemsPerPage,
    order: "",
  }
  message: string = "";
  public showloader: boolean = false;      
  private subscription: Subscription;
  timercount:any; 

  constructor(
    private roleService: RoleService
  ) { }

  ngOnInit(): void {
    this.findRoleWithSelectedColoumn(null)
    this.getUsers();
   
  }

  public setTimer(){
    this.showloader = true;
    this.timercount = timer(5000); // 5000 millisecond means 5 seconds
    this.timercount.subscribe(() => {
        this.showloader = false;
    });
  }

  findRoleWithSelectedColoumn(event: any) {
    this.roleService.findRoleWithSelectedColoumn(this.query)
      .subscribe((data: any) => {
        if (data) {
          this.roles = data.docs;

        }
        (error: any) => {
          console.log(error);


        }
      });

  }

  getUsers() {
    this.roleService.getUsers(this.query0)
      .subscribe((data: any) => {
        if (data) {
          this.usersList = data.docs;

        }
        (error: any) => {
          console.log(error);


        }
      });

  }


  checkAllCheckBox(ev: any) {
    this.usersList.forEach(x => x.isUserchecked = ev.target.checked)
  }
  isAllCheckBoxChecked() {
    return this.usersList.every(p => p.isUserchecked);
  }
  createRole() {
    for (let xUser of this.usersList) {
      if (xUser.isUserchecked == true) {
        this.userdetails["userinfo"] = xUser;

        this.roleService.createRole(this.userdetails)
          .subscribe((data: any) => {
            if (data) {
              this.setTimer();
              this.message = data.message;
            }
            (error: any) => {
              console.log(error);
            }
          });
      }

    }
  }

}
