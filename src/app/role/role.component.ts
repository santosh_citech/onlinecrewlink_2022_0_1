import { Component, OnInit } from '@angular/core';
import { RoleService } from "./role.service"

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {
  itemsPerPage: any = 10;
  currentPage: any = 1;
  public rolesList: any[] = [];
  query: any = {
    page: 1,
    size: 10,
    code:''
  }
  public isLoading: boolean = false;
  perPage: any;
  totalPages: any;
  totalRecords: any;



  constructor(
    private roleService: RoleService
  ) { }

  ngOnInit(): void {
    this.getroles(null);
  }

  getroles(event: any) {
    this.roleService.getroles(this.query)
      .subscribe((results: any) => {
          if (results) {
            this.rolesList = results.docs;
            this.currentPage = results.page;
            this.perPage = results.limit;
            this.totalPages = results.totalPages;
            this.totalRecords = results.totalDocs;

            this.isLoading = false;
          }
          (error: any) => {
            console.log(error);


          }
        });

  }
  selectRole(r: any) {

  }
  removeRole(r: any) {

  }

}
