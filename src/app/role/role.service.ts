import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError, map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  private apiUrl: string;

  private headers = new HttpHeaders()
    .set('content-type', 'application/json');
    httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
  constructor(private http: HttpClient) {
    this.apiUrl = 'http://localhost:4000'
  }

  createRole(roledetails: any = {}):Observable<any> {
    return this.http.post(this.apiUrl+'/api/v1/role/createrole',roledetails, this.httpOptions)

  }

  getUsers(query: any) {
    return this.http.get<any[]>(this.apiUrl + '/api/v1/admin/users', { params: query }).pipe(map((data: any) => data),
      catchError(error => 'error')
    );
  }


  findRoleWithSelectedColoumn(query: any) {
    return this.http.get<any[]>(this.apiUrl + '/api/v1/role/findRoleWithSelectedColoumn', { params: query }).pipe(map((data: any) => data),
      catchError(error => 'error')
    );
  }
  getroles(query: any) {
    return this.http.get<any[]>(this.apiUrl + '/api/v1/role/getroles', { params: query }).pipe(map((data: any) => data),
      catchError(error => 'error')
    );
  }

}
