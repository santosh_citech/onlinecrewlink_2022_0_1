import { NgModule } from '@angular/core';

import { RoleRoutingModule } from "./role.route"

import { RoleComponent } from "./role.component";

import { RoleService } from "./role.service";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CreateroleComponent } from './createrole/createrole.component';
@NgModule({
    imports: [RoleRoutingModule, CommonModule, FormsModule, ReactiveFormsModule],
    declarations: [RoleComponent, CreateroleComponent],
    exports: [RoleComponent],
    providers: [RoleService]

})
export class RoleModule { }