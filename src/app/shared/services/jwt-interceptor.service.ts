import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()


export class JwtInterceptor implements HttpInterceptor {
  currentUserOBJ = {
    token: 'fake-jwt-token',
    username: 'san@123'
  }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    var currentUser = localStorage.getItem('currentUser');
    // let currentUser = JSON.parse(storageUser);
    if (this.currentUserOBJ && this.currentUserOBJ.token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.currentUserOBJ.token}`
        }
      });
    }

    return next.handle(request);
  }
}