import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { User } from "../models/user.model"
import { map } from 'rxjs';


@Injectable()
export class AuthenticationService {
  apiUrl = 'http://localhost:4000';
  private headers = new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', '*');

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http: HttpClient) {


  }

  getUsers(query: any) {
    return this.http.get<any[]>(this.apiUrl + '/api/v1/admin/users', { params: query }).pipe(map((data: any) => data),
      catchError(error => 'error')
    );
  }

  resetUserPassword(model: any = {}) {
    return this.http.post(this.apiUrl + '/api/v1/admin/resetpassword ', model, this.httpOptions)
  }

  registerUser(model: any = {}): Observable<any> {
    return this.http.post(this.apiUrl + '/register', model, this.httpOptions)

  }

  login(username: string, password: string): Observable<any> {
    return this.http.post(this.apiUrl + '/login', { "username": username, "password": password }, this.httpOptions)

  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    window.sessionStorage.clear();


  }
  processError(err: any) {
    let message = '';
    if (err.error instanceof ErrorEvent) {
      message = err.error.message;
    } else {
      message = `Error Code: ${err.status}\nMessage: ${err.message}`;
    }
    console.log(message);
    return throwError(message);
  }


}