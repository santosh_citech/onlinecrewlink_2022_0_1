import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError,of,map } from 'rxjs';
import { User } from "../models/user.model"
@Injectable({
  providedIn: 'root'
})


export class UserService {
  apiUrl = 'http://localhost:3000';
  private headers = new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', '*');
  constructor(private http: HttpClient) { }
  getAll() {
    return this.http.get<any[]>(this.apiUrl + '/api/v1/getusers').pipe(map((data: any) => data.users),
      catchError(error => { return throwError('Its a Trap!') })
    );
  }

  getById(id: number) {
    return this.http.get(this.apiUrl + '/api/v1/getusers/' + id);
  }

  create(user: User) {
    return this.http.post<User>(this.apiUrl + '/api/v1/getusers', JSON.stringify(user), { 'headers': this.headers })
      .pipe(
        retry(1),
        catchError(this.processError)
      )
  }

  update(user: User) {
    // return this.http.put(this.apiUrl + '/userDataBase/' + user.id, user);
  }

  delete(id: number) {
    return this.http.delete(this.apiUrl + '/userDataBase/' + id);
  }

  getUsers(id: any): Observable<User> {
    return this.http.get<User>(this.apiUrl + '/userDataBase')
      .pipe(
        retry(1),
        catchError(this.processError)
      )
  }

  getSingleUser(id: any): Observable<User> {
    return this.http.get<User>(this.apiUrl + '/userDataBase/' + id)
      .pipe(
        retry(1),
        catchError(this.processError)
      )
  }

  addUser(data: any): Observable<User> {
    return this.http.post<User>(this.apiUrl + '/userDataBase', JSON.stringify(data), { 'headers': this.headers })
      .pipe(
        retry(1),
        catchError(this.processError)
      )
  }

  updateUser(id: any, data: any): Observable<User> {
    return this.http.put<User>(this.apiUrl + '/userDataBase/' + id, JSON.stringify(data), { 'headers': this.headers })
      .pipe(
        retry(1),
        catchError(this.processError)
      )
  }

  deleteUser(id: any) {
    return this.http.delete<User>(this.apiUrl + '/userDataBase/' + id, { 'headers': this.headers })
      .pipe(
        retry(1),
        catchError(this.processError)
      )
  }


  processError(err: any) {
    let message = '';
    if (err.error instanceof ErrorEvent) {
      message = err.error.message;
    } else {
      message = `Error Code: ${err.status}\nMessage: ${err.message}`;
    }
    console.log(message);
    return throwError(message);
  }
}
