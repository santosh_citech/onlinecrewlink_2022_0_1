import { Directive, Input, Renderer2, ElementRef } from '@angular/core';

@Directive({
    selector: '[resize]'
})

export class resizeDirective {

    @Input() nwidth: number = 0;
    constructor(
        private renderer: Renderer2,
        private elementRef: ElementRef,) { }

    ngOnInit() {
        this.renderer.setStyle(this.elementRef.nativeElement, 'width', `${this.nwidth}px`);
    }


}