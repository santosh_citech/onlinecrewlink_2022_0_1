import { Injectable } from '@angular/core';
import * as d3 from "d3";
@Injectable({
  providedIn: 'root'
})
export class ChartService {

  constructor() { }

  drawYAxis(graph: any, axisId: any, clas: any, x: any, side: any, height: any, ticks: any, click: any, mouseOver: any, mouseOut: any, dblClick: any, noOfDays: any) {
    var y0 = d3.scaleLinear()
      .domain([d3.timeDay.offset(new Date("Sun Mar 22 2015 24:00:00"), noOfDays - 1), new Date("Sun Mar 22 2015 00:00:00")])
      .range([height, 0]);
    var yAxis = d3.axisBottom(y0).ticks(ticks)
      .tickFormat(function (d: any) { return (ticks != 24) ? d3.timeFormat('%H:%M %a')(new Date(d)) : d3.timeFormat('%H:%M')(new Date(d)); });

    graph.append("g")
      .attr("class", clas)
      .attr("id", axisId)
      .attr("transform", "translate(" + x + ")")
      .on("dblclick", dblClick)
      .on("mouseover", mouseOver)
      .on("mouseout", mouseOut)
      .on("click", click)
      .call(yAxis);
  }

  addContentOnYAxis(graph: any, data: any, divClass: any, contentId: any, textClass: any, x: any, y: any, text: any,
    click: any, mouseOver: any, mouseOut: any, dblClick: any) {
    var id;
    graph.append("g")
      .attr("class", divClass)
      .selectAll(".text")
      .data(data).enter()
      .append("text")
      .attr("class", textClass)
      .attr("id", contentId)
      .attr("x", x)
      .attr("y", y)
      .text(text)
      .on("dblclick", dblClick)
      .on("mouseover", mouseOver)
      .on("mouseout", mouseOut)
      .on("click", click);
  }
  addYAxisHeading(graph: any, text: any, x: any, y: any, clas: any) {
    graph.append("g").attr("class", clas)
      .append("text").attr("x", x)
      .attr("y", y)
      .text(text);
  }
}
