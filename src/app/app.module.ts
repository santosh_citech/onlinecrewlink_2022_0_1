import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import * as $ from 'jquery';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


//// authentication
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { ForgetComponent } from './auth/forget/forget.component';

import { HomeComponent } from './home/home.component';

import { DashboardComponent } from './dashboard/dashboard.component';


/////  layout
import { NavbarComponent } from './layout/navbar/navbar.component';
import { SidebarNavbarComponent } from './layout/sidebar/sidebar.component';


import { FooterComponent } from './layout/footer/footer.component';

import { BarComponent } from './component/bar/bar.component';

import { LinechartComponent } from './component/linechart/linechart.component';
import { ClineChartComponent } from './component/cline-chart/cline-chart.component';



/// services
import { ChartService } from "./shared/chart_service/chart-service.service";
import { AuthenticationService } from "./shared/services/authentication.service";
import { UserService } from "./shared/services/user.service";
import { TrainTimeTableService } from "./component/timetable/traintimetable.service";




/// direc 
import { resizeDirective } from "./shared/directive/resize";
import { FileSizePipe } from './shared/pipes/filesize.pipe';
// guard 
import { AuthGuard } from "./shared/guards/auth-guard.guard";







import { PageFourhundredthreeComponent } from './errorpages/page-fourhundredthree/page-fourhundredthree.component';
import { PageFourhundredfourComponent } from './errorpages/page-fourhundredfour/page-fourhundredfour.component';
import { PageFivehundredComponent } from './errorpages/page-fivehundred/page-fivehundred.component';
import { BlankpageComponent } from './blankpage/blankpage.component';
import { LoadingSpinnerComponent } from './loading-spinner/LoadingSpinner.component';

import { ChartJSComponent } from './datapresentation/chart-js/chart-js.component';
import { ChartJS2Component } from './datapresentation/chart-js2/chart-js2.component';
import { MorisJSComponent } from './datapresentation/moris-js/moris-js.component';
import { EChartsComponent } from './datapresentation/echarts/echarts.component';
import { OtherChartsComponent } from './datapresentation/other-charts/other-charts.component';
import { RightsidebarComponent } from './layout/rightsidebar/rightsidebar.component';







import { MediagallaryComponent } from './ui_element/mediagallary/mediagallary.component';
import { GeneralElementComponent } from './ui_element/general-element/general-element.component';
import { TypographyComponent } from './ui_element/typography/typography.component';
import { WidgetsComponent } from './ui_element/widgets/widgets.component';
import { InvoiceComponent } from './ui_element/invoice/invoice.component';
import { InboxComponent } from './ui_element/inbox/inbox.component';
import { CalendarComponent } from './ui_element/calendar/calendar.component';







import { TimetableComponent } from './component/timetable/timetable.component';



// import {DrivingSectionComponentModule} from "./driving-sections/driving-sec.module"

/// custom loader 

 // import { LoadingSpinnerModule } from './loading-spinner/LoadingSpinner.module';

//  declare module "@angular/core" {
//   interface ModuleWithProviders<T = any> {
//     ngModule: Type<T>;
//     providers?: Provider[];
//   }
// }

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ForgetComponent,
    HomeComponent,
    DashboardComponent,

    NavbarComponent,
    SidebarNavbarComponent,
    FooterComponent,
    BarComponent,

    LinechartComponent,
    ClineChartComponent,
  
    resizeDirective,
   
    FileSizePipe,
    
    PageFourhundredthreeComponent,
    PageFourhundredfourComponent,
    PageFivehundredComponent,
    BlankpageComponent,
    LoadingSpinnerComponent,
   
    ChartJSComponent,
    ChartJS2Component,
    MorisJSComponent,
    EChartsComponent,
    OtherChartsComponent,
    RightsidebarComponent,
   
 
   
   

    MediagallaryComponent,
    GeneralElementComponent,
    TypographyComponent,

    WidgetsComponent,
    InvoiceComponent,
    InboxComponent,
    CalendarComponent,
    
  
    TimetableComponent,
    
    

  ],
  imports: [

    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
   // LoadingSpinnerModule.forRoot()
  ],
  providers: [

    ChartService,
    
    AuthenticationService,
    AuthGuard,
    UserService,
    TrainTimeTableService

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
