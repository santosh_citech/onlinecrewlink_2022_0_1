import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralElementComponent } from './general-element.component';

describe('GeneralElementComponent', () => {
  let component: GeneralElementComponent;
  let fixture: ComponentFixture<GeneralElementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GeneralElementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
