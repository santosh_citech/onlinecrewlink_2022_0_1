import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MediagallaryComponent } from './mediagallary.component';

describe('MediagallaryComponent', () => {
  let component: MediagallaryComponent;
  let fixture: ComponentFixture<MediagallaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MediagallaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MediagallaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
