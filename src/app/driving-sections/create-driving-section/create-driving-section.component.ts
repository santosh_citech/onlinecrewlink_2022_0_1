import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-create-driving-section',
  templateUrl: './create-driving-section.component.html',
  styleUrls: ['./create-driving-section.component.css']
})
export class createDrivingSectionComponent implements OnInit {

  result: any[] = [];
  constructor(private http: HttpClient) {
    this.getJSON().subscribe(data => {
      this.result = data;
    });
  }

  ngOnInit(): void {
  }



  public getJSON(): Observable<any> {
    return this.http.get("./assets/data/data.json");
  }

}
