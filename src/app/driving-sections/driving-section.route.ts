import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { createDrivingSectionComponent } from "./create-driving-section/create-driving-section.component";
import { ListDrivingSectionComponent } from "./list-driving-section/list-driving-section.component";

const routes: Routes = [
    { path:"create", component: createDrivingSectionComponent },
    { path:"list", component: ListDrivingSectionComponent }
];

@NgModule({
    exports: [RouterModule],
    imports:[RouterModule.forChild(routes)]
})

export class createDrivingSectionComponentRoutingModule{}