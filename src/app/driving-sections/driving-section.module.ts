import { NgModule } from '@angular/core';
import { createDrivingSectionComponentRoutingModule } from "./driving-section.route"

import { createDrivingSectionComponent } from "./create-driving-section/create-driving-section.component";
import { ListDrivingSectionComponent } from "./list-driving-section/list-driving-section.component";

@NgModule({
    imports: [createDrivingSectionComponentRoutingModule],
    declarations: [createDrivingSectionComponent, ListDrivingSectionComponent],
    exports: [createDrivingSectionComponent, ListDrivingSectionComponent],
    providers: []

})
export class CreateDrivingSectionModule { }