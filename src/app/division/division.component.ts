import {Component, OnInit } from '@angular/core';
import { DivisionService } from "./division.service"
@Component({
  selector: 'app-division',
  templateUrl: './division.component.html',
  styleUrls: ['./division.component.css']
})
export class DivisionComponent implements OnInit {




  public isLoading: boolean = false;
  public itemsPerPage: number = 10;
  public currentPage: number = 1;
  postsPerPage: number[] = [25, 50, 100];
  maxPages: number = 7;
  public query: any = {
    page: this.currentPage,
    limit: this.itemsPerPage,
    order: "",
    name: ""
  }

  public divisionResult: any[] = [];
  perPage: any;
  totalPages: any;
  totalRecords: any;
  constructor(

    private divService: DivisionService

  ) { }

  ngOnInit() {


    this.loadServerData(null);

  }

  pageChanged(event: any) {
    this.query.page = event.page;
    this.query.limit = event.itemsPerPage
    this.loadServerData(null,);
  }

  loadServerData(event: any) {

    console.log(event);

    this.isLoading = true;
    this.query.limit = event;
    this.divService.getdivisions(this.query)
      .subscribe(
        (results: any) => {
          if (results) {
            this.divisionResult = results.docs;
            this.currentPage = results.page;
            this.perPage = results.limit;
            this.totalPages = results.totalPages;
            this.totalRecords = results.totalDocs;

            this.isLoading = false;
          }
          (error: any) => {
            console.log(error);


          }
        });

  }

  updateDivision(division: object) {

  }
  removeDivision(division: object) {

  }

  public isSelected: boolean = false;
  isClicked() {
    this.isSelected = !this.isSelected;
  }
}
