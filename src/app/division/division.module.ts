import { NgModule } from '@angular/core';

import { DivisionRoutingModule } from "./division.route"

import { DivisionComponent } from "./division.component";
import { CreateDivisionComponent } from "./create-division/create-division.component";
import { DivisionGraphComponent } from "./division-graph/division-graph.component";
import {DivisionService} from "./division.service";
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';  
@NgModule({
    imports: [DivisionRoutingModule,CommonModule,FormsModule,ReactiveFormsModule],
    declarations: [DivisionComponent, CreateDivisionComponent, DivisionGraphComponent],
    exports: [DivisionComponent, CreateDivisionComponent, DivisionGraphComponent],
    providers: [DivisionService]

})
export class DivisionModule { }