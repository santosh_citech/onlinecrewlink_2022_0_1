import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";


import { DivisionComponent } from "./division.component";
import { CreateDivisionComponent } from "./create-division/create-division.component";
import { DivisionGraphComponent } from "./division-graph/division-graph.component";
const routes: Routes = [
    { path:"create", component: CreateDivisionComponent },
    { path:"list", component: DivisionComponent },
    { path:"graph", component: DivisionGraphComponent }
];

@NgModule({
    exports: [RouterModule],
    imports:[RouterModule.forChild(routes)]
})

export class DivisionRoutingModule{}