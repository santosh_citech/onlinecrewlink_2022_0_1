import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Page } from './pagination';
function forceToBoolean(input: string | boolean): boolean {
  return !!input && input !== 'false';
}


@Component({
  selector: 'app-pagination',
  templateUrl: 'pagination.component.html',
  styleUrls: ["./pagination.component.css"]
})



export class PaginationComponent implements OnInit {

  @Input() maxPages: number;
  @Input() current: number = 1;
  @Input() postsPerPage: number[];
  @Input() itemsPerPage: number = 10;

  @Input() totalPages: number;
  @Output() changePage = new EventEmitter();

  pages: any[] = [];
  _autoHide = false;
  pageModel: Page = {
    page: this.current,
    itemsPerPage: this.itemsPerPage
  };

  constructor() { }

  ngOnInit() {
    if (this.maxPages) {
      this.createPages();
    }
  }

  @Input()
  get autoHide(): boolean {
    return this._autoHide;
  }
  set autoHide(value: boolean) {
    this._autoHide = forceToBoolean(value);
  }
  setPage(page: number, perPage: number) {
    this.pageModel.page = page;
    this.pageModel.itemsPerPage = perPage;
    this.changePage.emit(this.pageModel);
  }

  createPages() {
    for (let i = 1; i <= this.maxPages; i++) {
      this.pages.push(i);
    }
  }
}  