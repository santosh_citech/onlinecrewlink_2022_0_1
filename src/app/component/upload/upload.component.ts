import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

  public uploadlist: any[] = [];
  current = new Date();

  constructor() {

  }

  ngOnInit(): void {
  }

  getExet(filename: any) {
    return filename.split('.').pop();
    
  }

  fileChange(event: any) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      let formData: FormData = new FormData();
      formData.append('uploadFile', file, file.name);
      this.uploadlist.push({

        title: file.name,
        creator: 'santosh',
        size: file.size,
        lastdate: new Date(file.lastModified),
        comment: "jkjkjk",
        type: file.type,
        status: "accepted"
      });



      /// let headers = new Headers();
      /** In Angular 5, including the header Content-Type can invalidate your request */
      /// headers.append('Content-Type', 'multipart/form-data');
      //// headers.append('Accept', 'application/json');
      // let options = new RequestOptions({ headers: headers });
      // this.http.post(`${this.apiEndPoint}`, formData, options)
      //     .map(res => res.json())
      //     .catch(error => Observable.throw(error))
      //     .subscribe(
      //         data => console.log('success'),
      //         error => console.log(error)
      //     )}
    }

  }

  deleteUpload(upload: any) {
    const index = this.uploadlist.indexOf(upload);
    if (index > -1) {
      this.uploadlist.splice(index, 1);
    }

  }

}
