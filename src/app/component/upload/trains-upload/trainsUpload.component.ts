import { Component, OnInit } from '@angular/core';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UploadService } from "../Upload.service";

@Component({
  selector: 'app-trainsUpload',
  templateUrl: './trainsUpload.component.html',
  styleUrls: ['./trainsUpload.component.css'],

})
export class TrainsUploadComponent implements OnInit {

  selectedFiles?: FileList;
  currentFile?: File;
  progress = 0;
  message = '';

  fileInfos?:any[];
  constructor(
    private uploadService: UploadService
  ) { }

  ngOnInit(): void {
    this.uploadService.getFiles().subscribe(
      (data: any) => {
        if (data) {
          this.fileInfos = data.result;
         
        }
        (error: any) => {
         

        }
      });
   
  }

  selectFile(event: any): void {
    this.selectedFiles = event.target.files;
  }

  upload(): void {
    this.progress = 0;

    if (this.selectedFiles) {
      const file: File | null = this.selectedFiles.item(0);

      if (file) {
        this.currentFile = file;

        this.uploadService.upload(this.currentFile).subscribe(
          (event: any) => {
            if (event.type === HttpEventType.UploadProgress) {
              this.progress = Math.round(100 * event.loaded / event.total);
            } else if (event instanceof HttpResponse) {
              this.message = event.body.message;
              this.uploadService.getFiles().subscribe(
                (results: any) => {
                  if (results) {
                    this.fileInfos = results;
                   
                  }
                  (error: any) => {
                   
          
                  }
                });
            }
          },
          (err: any) => {
            console.log(err);
            this.progress = 0;

            if (err.error && err.error.message) {
              this.message = err.error.message;
            } else {
              this.message = 'Could not upload the file!';
            }

            this.currentFile = undefined;
          });

      }

      this.selectedFiles = undefined;
    }
  }

  deleteUpload(upload: any) {
    const index = this.fileInfos.indexOf(upload);
    if (index > -1) {
      this.fileInfos.splice(index, 1);
    }

  }

}
