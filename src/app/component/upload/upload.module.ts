import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { UploadRoutingModule } from "./upload.route"

import { UploadComponent } from "./upload.component";
import { StationUploadComponent } from "./station-upload/stationupload.component";
import { TrainsUploadComponent } from "./trains-upload/trainsUpload.component";
import { DivisionUploadComponent } from "./division-upload/division-upload.component";
import { trainstationUploadComponent } from "./trainstation-upload/trainstationUpload.component";

import {UploadService} from "./Upload.service"

@NgModule({
    imports: [UploadRoutingModule, FormsModule, ReactiveFormsModule, CommonModule],
    declarations: [UploadComponent, StationUploadComponent,TrainsUploadComponent,DivisionUploadComponent,trainstationUploadComponent],
    exports: [UploadComponent, StationUploadComponent,TrainsUploadComponent,DivisionUploadComponent,trainstationUploadComponent],
    providers: [UploadService]

})
export class UploadModule { }