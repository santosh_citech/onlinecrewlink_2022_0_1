import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { UploadComponent } from "./upload.component";
import { StationUploadComponent } from "./station-upload/stationupload.component";
import { TrainsUploadComponent } from "./trains-upload/trainsUpload.component";

import { DivisionUploadComponent } from "./division-upload/division-upload.component";
import { trainstationUploadComponent } from "./trainstation-upload/trainstationUpload.component";
const routes: Routes = [
    { path: "station-upload", component: StationUploadComponent },
    { path: "trains-upload", component: TrainsUploadComponent },
    { path: "trainstation-upload", component: trainstationUploadComponent },
    { path: "division-upload", component: DivisionUploadComponent },
    { path: "list", component: UploadComponent }
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class UploadRoutingModule { }