import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError, map } from 'rxjs';
import { HttpHeaders, HttpParams } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class UploadService {
    private baseUrl = 'http://localhost:4000';
     headers:any = new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: 'application/json',
      })

    constructor(private http: HttpClient) { }

    upload(file: File): Observable<HttpEvent<any>> {
        const formData: FormData = new FormData();

        formData.append('file', file);

        const req = new HttpRequest('POST', `${this.baseUrl}/api/v1/trainupload`, formData, {
         
            reportProgress: true,
            responseType: 'json'
        });

        return this.http.request(req);
    }

    getFiles() {
        return this.http.get(`${this.baseUrl}/api/v1/Upload`).pipe(map((data: any) => data),
        catchError(error => 'error')
      );
    }
}