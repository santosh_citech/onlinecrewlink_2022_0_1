import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatetrainComponent } from './createtrain.component';

describe('CreatetrainComponent', () => {
  let component: CreatetrainComponent;
  let fixture: ComponentFixture<CreatetrainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreatetrainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatetrainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
