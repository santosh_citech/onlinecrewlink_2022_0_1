import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';  

import { TrainRoutingModule } from "./train.route"
import { TrainsComponent } from "./trains.component";
import { CreateTrainComponent } from "./createtrain/createtrain.component";
import {PaginationComponent} from "../../pagination/pagination.component"
import {TrainService} from "./train.service";

@NgModule({
    imports: [TrainRoutingModule,CommonModule,FormsModule,ReactiveFormsModule],
    declarations: [TrainsComponent, CreateTrainComponent,PaginationComponent],
    exports: [TrainsComponent, CreateTrainComponent,PaginationComponent],
    providers: [TrainService]

})
export class TrainModule { }