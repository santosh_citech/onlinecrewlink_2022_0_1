import {
  Component, OnInit, AfterViewChecked,
  ChangeDetectorRef,
  EventEmitter,
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TrainService } from "./train.service"
import { Traintype } from "./type";
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-trains',
  templateUrl: './trains.component.html',
  styleUrls: ['./trains.component.css']
})


export class TrainsComponent implements OnInit, AfterViewChecked {

  public trainLists: Traintype[] = [];
  public selectedQuantity: any;
  public days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
  form!: FormGroup;

  searchTerm: string = '';
  reload: EventEmitter<boolean> = new EventEmitter();
  isLoading: boolean = false;


  selectedCssClass = 'selected-train-section';

  public itemsPerPage: number = 10;
  public currentPage: number = 1;
  postsPerPage: number[] = [5, 10, 25, 50, 100];
  maxPages: number = 5;

  query: any = {
    page: this.currentPage,
    limit: this.itemsPerPage,
    order: "",
    trainNumber: "",
    trainname: "",
    fromstation: "",
    tostation: ""
  }

  perPage: any;
  totalPages: any;
  totalRecords: any;

  constructor(
    private trainService: TrainService,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private _router: Router
  ) { }

  ngOnInit(): void {
    // this.load();
    this.loadTrainsList(10);
    this.buildForm();
  }

  ngAfterViewChecked(): void {
    this.cdr.detectChanges();
  }

  pageChanged(event: any) {
    this.query.page = event.page;
    this.query.limit = event.itemsPerPage
    this.loadTrainsList(null);
  }

  trainTimeTable(train: any = {}) {

    this._router.navigate(['home/timetable'], { skipLocationChange: true, queryParams: { "trainNo": train.trainNo, "startDay": train.startDay, } });

  }

  loadTrainsList(event: any) {
    this.isLoading = true;
    this.trainService.gettrains(this.query)
      .subscribe(
        (results: any) => {
          if (results) {
            this.trainLists = results.docs;
            this.currentPage = results.page;
            this.perPage = results.limit;
            this.totalPages = results.totalPages;
            this.maxPages = results.totalPages;
            this.totalRecords = results.totalDocs;
            this.isLoading = false;
          }
          (error: any) => {
            this.isLoading = false

          }
        });


  }



  // Init form
  buildForm() {
    this.form = this.fb.group({
      term: ['', [Validators.required]],
      recordsPerPage: [''],
    });
  }

  // On form submit => assign search term
  submitForm(): void {
    if (this.form.invalid) {
      return;
    }
    this.searchTerm = this.form.value.term;
  }

  // Clear search results on search box empty
  clearSearchResult() {
    // if (this.form.controls.term.value === '' && this.searchTerm) {
    //   this.searchTerm = '';
    //   setTimeout(() => {
    //     this.reload.emit(true);
    //     this.reload.emit(false);
    //   }, 100);
    // }
  }

  getSelectedTrainCss = function (trainItem: any) {
    // if (selectedTrain.trainNo == trainItem[11013]
    //     && $scope.selectedTrain.startDay == Days[trainItem[$scope.trains.fields.startDay]]) {
    //   return $scope.selectedTrain.cssClass;
    // }
    // return this.selectedCssClass;
    return "";
  };

}
