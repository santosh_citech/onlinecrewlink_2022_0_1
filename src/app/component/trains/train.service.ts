import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError, map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TrainService {
  private apiUrl: string;

  private headers = new HttpHeaders()
    .set('content-type', 'application/json');
  constructor(private http: HttpClient) {
    this.apiUrl = 'http://localhost:4000'
  }

  loadServerData(): Observable<any> {
    return this.http.get(this.apiUrl, { responseType: 'text' })

  }

  gettrains(query: any) {
    return this.http.get<any[]>(this.apiUrl + '/api/v1/gettrains', { params: query }).pipe(map((data: any) => data),
      catchError(error => 'error')
    );
  }

}
