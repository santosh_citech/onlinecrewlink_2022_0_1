import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import * as d3 from "d3";
import { ChartService } from "../../shared/chart_service/chart-service.service"
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';

@Component({
  selector: 'app-linechart',
  templateUrl: './linechart.component.html',
  styleUrls: ['./linechart.component.css']
})
export class LinechartComponent implements OnInit {
  public n: number;
  public chartId: any;
  private Days = ["SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"];
  private margin: any = {};
  private divWidth: number = 0;
  private divheight: number= 0;
  private trainStations: any[] = [];
  private yAxisXValue: any;

  private svg: any;
  private width: any
  private height: any;
  private apiUrl: string;
  private finalWidth: any;
  private finalHeight: any;
  private graph: any;
  private chartHeading = "section chart";
  private dimensionsYAxis: any;

  // @ViewChild('lineChartId')
  // public lineChartId: ElementRef;

  constructor(private http: HttpClient, private el: ElementRef, private chartService: ChartService) {
    this.n = new Date().getTime();
    this.chartId = "sectionChartId" + this.n;
    this.apiUrl = 'assets/data/combine_train_stations_1_2_3_with_loco_type.csv'
    let row = [];
    let csvToRowArray = [];
    this.loadServerData().subscribe((response) => {
      csvToRowArray = response.split("\n");
      for (let index = 1; index < 101 - 1; index++) {

        row = csvToRowArray[index].split(",");

        this.trainStations.push({
          "slNo": row[1],
          "trainNo": row[0],
          "stationcode": row[2],
          "dayofjourney": row[3],
          "arrivalTime": row[4],
          "departureTime": row[5],
          "distance": row[6],
          "locoType": row[7]

        })
      }
      this.initializeValues(this.trainStations);
      this.loadChart();
    })

  }

  ngOnInit(): void {

  }


  initializeValues(trainstation: [][]) {
    this.margin = { top: 50, left: 30, bottom: 40, right: 40 };
    this.width = (trainstation.length - 1) - this.margin.left - this.margin.right;
    this.height = (trainstation.length - 1) * 50 - this.margin.top - this.margin.bottom;
    this.finalWidth = this.width + this.margin.left + this.margin.right;
    this.finalHeight = this.height + this.margin.top + this.margin.bottom + 100;
   // this.yAxisXValue = this.divWidth * 0.50;
    this.dimensionsYAxis = d3.scaleLinear().range([0, this.height]).domain([0, this.trainStations.length - 1]);
  }
  loadChart() {
   // this.lineChartId.nativeElement.innerHTML = "";
    this.graph = d3.select("#" + this.chartId).append("svg").attr("width", this.finalWidth).attr("height", this.finalHeight)
      .append("g").attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");


    this.chartService.drawYAxis(this.graph, "", "y-axis", this.yAxisXValue, 'left', this.height, 0,
      function () {

      },
      function (d: any) {

      }, function (d: any) {

      }, function (d: any) {

      }, 20);


    this.chartService.addYAxisHeading(this.graph, this.chartHeading, this.yAxisXValue - 50,
      function (d: any) {
        // return this.dimensionsYAxis(0) - 30
      },

      "headingtext");


  }

  loadServerData(): Observable<any> {
    return this.http.get(this.apiUrl, { responseType: 'text' });

  }



  ngAfterViewInit() {
    // this.buildSVG();
    //this.drawBars(this.data);
    // this.divWidth = this.lineChartId.nativeElement.offsetWidth;
    // this.divheight = this.lineChartId.nativeElement.offsetHeight;
    // console.log(this.divWidth);
   

  }



  private buildSVG(): void {
    this.svg = d3.select("#" + this.chartId)
      .append("svg")
      .attr("width", this.width + (this.margin * 2))
      .attr("height", this.height + (this.margin * 2))
      .append("g")
      .attr("transform", "translate(" + this.margin + "," + this.margin + ")");
  }

}
