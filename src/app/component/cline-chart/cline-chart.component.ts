import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import * as c3 from 'c3';
@Component({
  selector: 'app-cline-chart',
  templateUrl: './cline-chart.component.html',
  styleUrls: ['./cline-chart.component.css']
})
export class ClineChartComponent implements OnInit {
  public c3chartId: any;
  public n: number;
  private chart: any;
  constructor() {
    this.n = new Date().getTime();
    this.c3chartId = "linechartId" + this.n;
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.drawC3Chart();
  }
  drawC3Chart() {

    this.chart = c3.generate({
      bindto: "#" + this.c3chartId,
      data: {
        columns: [
          ['data1', 30, 200, 100, 400, 150, 250],
          ['data2', 50, 20, 10, 40, 15, 25]
        ]
      }
    });

    setTimeout(() => {
      this.chart.load({
        columns: [
          ['data1', 230, 190, 300, 500, 300, 400]
        ]
      });
    }, 1000);


    setTimeout(() => {
      this.chart.load({
        columns: [
          ['data3', 130, 150, 200, 300, 200, 100]
        ]
      });
    }, 1500);

    setTimeout(() => {
      this.chart.unload({
        ids: 'data1'
      });
    }, 2000);

  }

}
