import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClineChartComponent } from './cline-chart.component';

describe('ClineChartComponent', () => {
  let component: ClineChartComponent;
  let fixture: ComponentFixture<ClineChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClineChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClineChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
