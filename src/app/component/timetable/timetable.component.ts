import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TrainTimeTableService } from "./traintimetable.service";
@Component({
  selector: 'app-timetable',
  templateUrl: './timetable.component.html',
  styleUrls: ['./timetable.component.css']
})
export class TimetableComponent implements OnInit {


  public isLoading: boolean = false;
  public itemsPerPage: number = 100;
  public currentPage: number = 1;
  public query: any = {
    page: this.currentPage,
    limit: this.itemsPerPage,
    order: "",
    trainno: "",
    startDays: []
  }

  selectedTrainStations: any = {};
  public trainStationsResult: any[] = [];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private trainTimeTableService: TrainTimeTableService

  ) { }

  ngOnInit() {
    this.route
      .queryParams
      .subscribe(params => {
        this.query.trainno = params['trainNo'];
        this.query.startDays = params['startDay'];

      });

    this.loadServerData();

  }

  loadServerData() {

    this.isLoading = true;
    this.trainTimeTableService.gettimetable(this.query)
      .subscribe(
        (results: any) => {
          if (results) {
            this.trainStationsResult = results.docs;
            console.log(this.trainStationsResult);
            this.isLoading = false;
          }
          (error: any) => {
            console.log(error);


          }
        });

  }

}
