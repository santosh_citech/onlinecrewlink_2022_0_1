import { Component, OnInit } from '@angular/core';
import { StationService } from './station.service';


@Component({
  selector: 'app-station',
  templateUrl: './station.component.html',
  styleUrls: ['./station.component.css']
})
export class StationComponent implements OnInit {

  itemsPerPage: number = 10;
  currentPage: number = 1;
  stations: any[] = [];
  hasAdmin: boolean = true;
  isLoading: boolean = false;
  query: any = {
    page: this.currentPage,
    limit: this.itemsPerPage,
    order: "",
    code: "",
    name: ""

  }

  constructor(private stationService: StationService) { }

  ngOnInit(): void {


    this.loadStations(null);
  }


  loadStations(event: any) {

    this.isLoading = true;
    this.stationService.getstations(this.query)
      .subscribe(
        (results: any) => {
          if (results) {
            this.stations = results.docs;
            this.isLoading = false;
          }
          (error: any) => {
            console.log(error);


          }
        });

  }
  removeStation(station: any) {

  }

  selectStation(station: any): void {

  }

}
