import { NgModule } from '@angular/core';
import { StationRoutingModule } from "./station.route"

import { CreateStationComponent } from "./create-station/create-station.component";
import { StationComponent } from "./station.component";
import {StationService} from "./station.service"

import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common'; 
@NgModule({
    imports: [StationRoutingModule,FormsModule,ReactiveFormsModule,CommonModule],
    declarations: [CreateStationComponent, StationComponent],
    exports:[CreateStationComponent, StationComponent],
    providers: [StationService]

})
export class StationModule { }