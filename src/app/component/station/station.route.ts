import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { CreateStationComponent } from "./create-station/create-station.component";
import { StationComponent } from "./station.component";
const routes: Routes = [
    { path:"create", component: CreateStationComponent },
    { path:"list", component: StationComponent }
];

@NgModule({
    exports: [RouterModule],
    imports:[RouterModule.forChild(routes)]
})

export class StationRoutingModule{}