import { NgModule, ModuleWithProviders } from '@angular/core';
import { LoadingSpinnerService } from './LoadingSpinner.service';
import { LoadingSpinnerComponent } from './LoadingSpinner.component';

export * from './LoadingSpinner.service';
export * from './LoadingSpinner.component';

@NgModule({
  imports: [],
  declarations: [LoadingSpinnerComponent],
  exports: [LoadingSpinnerComponent],
  providers: [LoadingSpinnerService]
})
export class LoadingSpinnerModule {
  static forRoot(): ModuleWithProviders<LoadingSpinnerModule> {
    return {
      ngModule: LoadingSpinnerModule,
      providers: [LoadingSpinnerService]
    };
  }
}