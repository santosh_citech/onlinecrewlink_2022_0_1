import { Component, OnInit, OnDestroy, Input, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoadingSpinnerService } from './LoadingSpinner.service';
@Component({
  selector: 'app-loading-spinner',
  templateUrl: './LoadingSpinner.component.html',
  styleUrls: ['./LoadingSpinner.component.css']
})
export class LoadingSpinnerComponent implements OnInit,OnDestroy  {
  showSpinner = false;
  _loadingText = '';
  _zIndex = 9999;
  _template =
    `<div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>`;
  
    subscription: Subscription;
    constructor(
    private spinnerService: LoadingSpinnerService
  ) { }

  ngOnInit(): void {
  }
  @Input()
  public set loadingText(value: string) {
    this._loadingText = value;
  }

  public get loadingText(): string {
    return this._loadingText;
  }

  @Input() public set zIndex(value: number) {
    this._zIndex = value;
  }


  public get zIndex(): number {
    return this._zIndex;
  }

  @Input()
  public set template(value: string) {
    this._template = value;
  }
  public get template(): string {
    return this._template;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
