import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from "./auth/login/login.component"
import { RegisterComponent } from "./auth/register/register.component"
import { ForgetComponent } from "./auth/forget/forget.component"
import { HomeComponent } from "./home/home.component"

import { BarComponent } from "./component/bar/bar.component"
import { LinechartComponent } from "./component/linechart/linechart.component"
import { ClineChartComponent } from "./component/cline-chart/cline-chart.component"


import { AuthGuard } from './shared/guards/auth-guard.guard';

import { UploadComponent } from "./component/upload/upload.component"
import { DashboardComponent } from "./dashboard/dashboard.component"

import { BlankComponent } from "./component/blank/blank.component";

// all errors pages 

import { PageFivehundredComponent } from "./errorpages/page-fivehundred/page-fivehundred.component";
import { PageFourhundredfourComponent } from "./errorpages/page-fourhundredfour/page-fourhundredfour.component";
import { PageFourhundredthreeComponent } from "./errorpages/page-fourhundredthree/page-fourhundredthree.component";

// blank page
import { BlankpageComponent } from "./blankpage/blankpage.component";



//// charts
import { ChartJSComponent } from "./datapresentation/chart-js/chart-js.component";
import { ChartJS2Component } from "./datapresentation/chart-js2/chart-js2.component";
import { MorisJSComponent } from "./datapresentation/moris-js/moris-js.component";
import { EChartsComponent } from "./datapresentation/echarts/echarts.component";
import { OtherChartsComponent } from "./datapresentation/other-charts/other-charts.component";





/// ui element


import { MediagallaryComponent } from "./ui_element/mediagallary/mediagallary.component";
import { GeneralElementComponent } from './ui_element/general-element/general-element.component';




// timetable
import { TimetableComponent } from "./component/timetable/timetable.component";







const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'forgetpassword/:id', component: ForgetComponent },
  { path: 'page_500', component: PageFivehundredComponent },
  { path: 'page_404', component: PageFourhundredfourComponent },
  { path: 'page_403', component: PageFourhundredthreeComponent },
  { path: "blankpage", component: BlankpageComponent },

  {
    path: "chat",
    loadChildren: () => import("./chat/chat.module").then(y => y.ChatModule)
  },

  {
    path: "timeline",
    loadChildren: () => import("./timeline/timeline.module").then(y => y.TimeLineModule)
  },

  {
    path: 'home', component: HomeComponent, canActivate: [AuthGuard],


    children: [

      { path: 'dashboard', component: DashboardComponent },
      {
        path: "drivingsection",
        loadChildren: () => import("./driving-sections/driving-section.module").then(x => x.CreateDrivingSectionModule)
      },
      {
        path: "division",
        loadChildren: () => import("./division/division.module").then(x => x.DivisionModule)
      },


      {
        path: "station",
        loadChildren: () => import("./component/station/station.module").then(x => x.StationModule)
      },

      {
        path: "trains",
        loadChildren: () => import("./component/trains/train.module").then(x => x.TrainModule)
      },
      {
        path: 'upload',
        loadChildren: () => import("./component/upload/upload.module").then(x => x.UploadModule)
      },
      {
        path: 'role',
        loadChildren: () => import("./role/role.module").then(x => x.RoleModule)
      },
      { path: 'barchart', component: BarComponent },
      { path: 'linechart', component: LinechartComponent },

      { path: "clinechart", component: ClineChartComponent },
      { path: "blank", component: BlankComponent },




      { path: 'timetable', component: TimetableComponent },


      {
        path: "datapresentation",

        children: [
          { path: 'chartJS', component: ChartJSComponent },
          { path: 'chartJS2', component: ChartJS2Component },
          { path: 'MorisJS', component: MorisJSComponent },
          { path: 'ECharts', component: EChartsComponent },
          { path: 'otherCharts', component: OtherChartsComponent },

        ]
      },

      {
        path: "uielement",

        children: [
          { path: 'generalelement', component: GeneralElementComponent },
          { path: 'mediagallary', component: MediagallaryComponent },
        ]
      },
      { path: '**', redirectTo: 'home', pathMatch: 'full' }

    ]
  }];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
