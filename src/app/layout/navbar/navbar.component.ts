import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../shared/services/authentication.service';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, AfterViewInit {


  loading = false;
  returnUrl: string = "";
  username:string = "santosh";
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService

  ) {

  }

  ngOnInit(): void {

  }

  logout(event: Event): void {
    event.preventDefault();
    console.log("At logout page !!")
    this.authenticationService.logout();
    this.router.navigate(['/login']);
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

  }

  reloadPage(): void {
    window.location.reload();
  }

  ngAfterViewInit(): void {
    $(window).on('load', function () {

      var toggle = false;

      $('#menutoggle').on("click", () => {
        toggle = !toggle;

        if (toggle) {
          $('#main_area').animate({ "margin-left": 0 + "px" });
          $('#top_navigation').animate({ "margin-left": 0 + "px" });
          //$('#sidebar_area').animate({ "margin-left": 0 + "px", position: "absolute", opacity: '0.5' });
          $('#sidebar_area').fadeOut("slow")


        }
        else {
          $('#main_area').animate({ "margin-left": 230 + "px" });
          $('#sidebar_area').animate({ "margin-right": 0 + "px" });
          $('#top_navigation').animate({ "margin-left": 230 + "px" });
          $('#sidebar_area').fadeIn("slow");
        }

      });
    });


  }



}
