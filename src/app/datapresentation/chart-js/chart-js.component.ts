import { Component, OnInit } from '@angular/core';
import { ElementRef, HostListener } from '@angular/core';
import { AfterViewInit } from '@angular/core'
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Chart, registerables } from "chart.js";
import ChartDataLabels from 'chartjs-plugin-datalabels';

declare var $: any;


@Component({
  selector: 'app-chart-js',
  templateUrl: './chart-js.component.html',
  styleUrls: ['./chart-js.component.css']
})
export class ChartJSComponent implements OnInit, AfterViewInit {
  chart: Chart;
  constructor() { }



  barChart() {
    Chart.register(...registerables);
    if ($('#mybarChart').length) {

      const canvasline = document.getElementById('mybarChart') as HTMLCanvasElement;
      const ctxcanvasline = canvasline.getContext('2d');
      new Chart(ctxcanvasline, {
        type: 'bar',
        data: {
          labels: ["January", "February", "March", "April", "May", "June", "July"],
          datasets: [{
            label: '# of Votes',
            backgroundColor: "#26B99A",
            data: [51, 30, 40, 28, 92, 50, 45]
          }, {
            label: '# of Votes',
            backgroundColor: "#03586A",
            data: [41, 56, 25, 48, 72, 34, 12]
          }]
        },

        options: {
          scales: {
            // yAxes: [{
            //     ticks: {
            //         beginAtZero: true
            //     }
            // }]
          }
        }
      });

    }

  }

  RadarChart() {
    // Radar Chart
    Chart.register(...registerables);
    if ($('#canvasRadar').length) {

      var canavasRadar = document.getElementById("canvasRadar") as HTMLCanvasElement;
      var ctxcanavasRadar = canavasRadar.getContext("2d");

      var data = {
        labels: ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"],
        datasets: [{
          label: "My First dataset",
          backgroundColor: "rgba(3, 88, 106, 0.2)",
          borderColor: "rgba(3, 88, 106, 0.80)",
          pointBorderColor: "rgba(3, 88, 106, 0.80)",
          pointBackgroundColor: "rgba(3, 88, 106, 0.80)",
          pointHoverBackgroundColor: "#fff",
          pointHoverBorderColor: "rgba(220,220,220,1)",
          data: [65, 59, 90, 81, 56, 55, 40]
        }, {
          label: "My Second dataset",
          backgroundColor: "rgba(38, 185, 154, 0.2)",
          borderColor: "rgba(38, 185, 154, 0.85)",
          pointColor: "rgba(38, 185, 154, 0.85)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(151,187,205,1)",
          data: [28, 48, 40, 19, 96, 27, 100]
        }]
      };

      new Chart(ctxcanavasRadar, {
        type: 'radar',
        data: data,
      });

    }

  }


  line_charts() {
    Chart.register(...registerables);
    console.log('run_charts  typeof [' + typeof (Chart) + ']');

    if (typeof (Chart) === 'undefined') { return; }

    console.log('init_charts');
    if ($('#lineChart').length) {
      const canvaslineChart = document.getElementById('lineChart') as HTMLCanvasElement;
      const ctxcanvaslineChart = canvaslineChart.getContext('2d');
      new Chart(ctxcanvaslineChart, {
        type: 'line',
        data: {
          labels: ["January", "February", "March", "April", "May", "June", "July"],
          datasets: [{
            label: "My First dataset",
            backgroundColor: "rgba(38, 185, 154, 0.31)",
            borderColor: "rgba(38, 185, 154, 0.7)",
            pointBorderColor: "rgba(38, 185, 154, 0.7)",
            pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointBorderWidth: 1,
            data: [31, 74, 6, 39, 20, 85, 7]
          }, {
            label: "My Second dataset",
            backgroundColor: "rgba(3, 88, 106, 0.3)",
            borderColor: "rgba(3, 88, 106, 0.70)",
            pointBorderColor: "rgba(3, 88, 106, 0.70)",
            pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(151,187,205,1)",
            pointBorderWidth: 1,
            data: [82, 23, 66, 9, 99, 4, 2]
          }]
        },
      });

    }
  }


  pieChart() {
    // Pie chart
    if ($('#pieChart').length) {
      const canvasPieChart = document.getElementById('pieChart') as HTMLCanvasElement;
      const ctxPieChart = canvasPieChart.getContext('2d');
      var data = {
        datasets: [{
          data: [120, 50, 140, 180, 100],
          backgroundColor: [
            "#455C73",
            "#9B59B6",
            "#BDC3C7",
            "#26B99A",
            "#3498DB"
          ],
          label: 'My dataset' // for legend
        }],
        labels: [
          "Dark Gray",
          "Purple",
          "Gray",
          "Green",
          "Blue"
        ]
      };

      new Chart(ctxPieChart, {
        data: data,
        type: 'pie',
        options: {
          responsive: true,
          plugins: {
            legend: {
              position: 'top',
            },
            title: {
              display: true,
              text: 'Chart.js Pie Chart'
            }
          }
        },
      });

    }

  }

  polarAreaChart() {
    if ($('#polarArea').length) {
      const canvasPolarArea = document.getElementById('polarArea') as HTMLCanvasElement;
      const ctxPolarArea = canvasPolarArea.getContext('2d');
      var data = {
        datasets: [{
          data: [342, 323, 333, 352, 361, 299, 272, 423, 425, 400, 382, 363],
          backgroundColor: [
            "#455C73",
            "#9B59B6",
            "#BDC3C7",
            "#26B99A",
            "#3498DB"
          ],
          label: 'Dataset',
          borderWidth: 4,
          hoverBorderColor: "white"
        }],
        labels: [
          "JAN",
          "FEB",
          "MAR",
          "APR",
          "MAY",
          "JUN",
          "JUL",
          "AUG",
          "SEP",
          "OCT",
          "NOV",
          "DEC"
        ]
      };

      new Chart(ctxPolarArea, {
        type: 'polarArea',
        data: data,
        options: {
          responsive: true,
          plugins: {
            datalabels: {
              formatter: function (value, context) {
                return context.chart.data.labels[context.dataIndex];
              },
              anchor: 'start',
              align: 'end',
              offset: 0 // Gets updated
            },
            title: {
              display: true,
              text: 'PolarChart with Tick Configuration'
            },
            legend: {
              display: false
            },





          },



        }
      });

    }
  }


  doughnutChart() {

    if ($('#canvasDoughnut').length) {
      const canvasDoughnut = document.getElementById('canvasDoughnut') as HTMLCanvasElement;
      const ctxCanvasDoughnut = canvasDoughnut.getContext('2d');
      var data = {
        labels: [
          "Dark Grey",
          "Purple Color",
          "Gray Color",
          "Green Color",
          "Blue Color"
        ],
        datasets: [{
          data: [120, 50, 140, 180, 100],
          backgroundColor: [
            "#455C73",
            "#9B59B6",
            "#BDC3C7",
            "#26B99A",
            "#3498DB"
          ],
          hoverBackgroundColor: [
            "#34495E",
            "#B370CF",
            "#CFD4D8",
            "#36CAAB",
            "#49A9EA"
          ]

        }]
      };

      new Chart(ctxCanvasDoughnut, {
        type: 'doughnut',
        data: data,
        options: {
          responsive: true,
          plugins: {
            legend: {
              position: 'top',
            },
            title: {
              display: true,
              text: 'Chart.js Doughnut Chart'
            }
          }
        },
      });

    }

  }


  ngAfterViewInit() {

    this.line_charts();
    this.barChart();
    this.RadarChart();
    this.pieChart();
    this.polarAreaChart();
    this.doughnutChart();
    $(document).ready(() => {
    })

  }

  ngOnInit() {
  }

}
