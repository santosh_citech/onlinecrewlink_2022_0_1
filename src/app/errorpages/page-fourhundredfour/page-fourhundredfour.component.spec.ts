import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageFourhundredfourComponent } from './page-fourhundredfour.component';

describe('PageFourhundredfourComponent', () => {
  let component: PageFourhundredfourComponent;
  let fixture: ComponentFixture<PageFourhundredfourComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageFourhundredfourComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageFourhundredfourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
