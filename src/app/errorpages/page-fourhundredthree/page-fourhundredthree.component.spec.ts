import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageFourhundredthreeComponent } from './page-fourhundredthree.component';

describe('PageFourhundredthreeComponent', () => {
  let component: PageFourhundredthreeComponent;
  let fixture: ComponentFixture<PageFourhundredthreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageFourhundredthreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageFourhundredthreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
