import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageFivehundredComponent } from './page-fivehundred.component';

describe('PageFivehundredComponent', () => {
  let component: PageFivehundredComponent;
  let fixture: ComponentFixture<PageFivehundredComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageFivehundredComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageFivehundredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
