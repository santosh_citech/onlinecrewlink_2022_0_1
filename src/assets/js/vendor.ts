

import "../vendors/jquery/dist/jquery.min.js";

import "../vendors/bootstrap/dist/js/bootstrap.bundle.min.js";

import "../vendors/fastclick/lib/fastclick.js";

import "../vendors/nprogress/nprogress.js";

import "../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js";
import "../vendors/jquery.hotkeys/jquery.hotkeys.js";
import "../vendors/google-code-prettify/src/prettify.js";

import "../vendors/skycons/skycons.js";


import "../vendors/jquery-sparkline/dist/jquery.sparkline.min.js";

import "../vendors/Flot/jquery.flot.js";
import "../vendors/Flot/jquery.flot.pie.js";
import "../vendors/Flot/jquery.flot.time.js";
import "../vendors/Flot/jquery.flot.stack.js";
import "../vendors/Flot/jquery.flot.resize.js";

import "../vendors/flot.orderbars/js/jquery.flot.orderBars.js";
import "../vendors/flot-spline/js/jquery.flot.spline.min.js";
import "../vendors/flot.curvedlines/curvedLines.js";

// import "../vendors/DateJS/build/date.js";

// import "../vendors/moment/min/moment.min.js";
// import "../vendors/bootstrap-daterangepicker/daterangepicker.js";





import "../vendors/Chart.js/dist/Chart.min.js";

import "../vendors/raphael/raphael.min.js";
import "../vendors/morris.js/morris.min.js";

import "../vendors/gauge.js/dist/gauge.min.js";

import "../vendors/echarts/dist/echarts.min.js";
import "../vendors/echarts/map/js/world.js";



import "../vendors/jqvmap/dist/jquery.vmap.js";
import "../vendors/jqvmap/dist/maps/jquery.vmap.world.js";
import "../vendors/jqvmap/dist/maps/jquery.vmap.usa.js";
import "../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js";

import "../vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js";